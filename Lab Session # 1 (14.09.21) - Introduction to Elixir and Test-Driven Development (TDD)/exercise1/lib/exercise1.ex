defmodule Exercise1 do
  def reverse(list), do: reverse(list, [])
  def reverse([], reversed), do: reversed
  def reverse([head | tail], reversed), do: reverse(tail, [head | reversed])

  def upcase([]), do: []
  def upcase([head | tail]) when head in ?a .. ?z, do: [head - 32 | upcase(tail)]
  def upcase([head | tail]), do: [head | upcase(tail)]

  def remove_non_alpha([]), do: []
  def remove_non_alpha([head | tail]) when head not in ?a .. ?z and head not in ?A .. ?Z, do: remove_non_alpha(tail)
  def remove_non_alpha([head | tail]), do: [head | remove_non_alpha(tail)]

  def compare(word), do: word == reverse(word)

  def palindrome([]), do: true
  def palindrome(word) do
    remove_non_alpha(word)
    |> upcase
    |> compare
  end

end
