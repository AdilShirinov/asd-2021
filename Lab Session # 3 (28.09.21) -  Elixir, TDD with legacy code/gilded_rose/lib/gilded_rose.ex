defmodule GildedRose do

  def increase_quality(item, amount), do: %{item | quality: min(50, item.quality + amount)}
  def decrease_quality(item, amount), do: %{item | quality: max(0, item.quality - amount)}
  def update_sell_in(item), do: %{item | sell_in: item.sell_in - 1}

  def update_quality(%{sell_in: sell_in} = item) when sell_in <= 0, do: update_quality(item, 2)
  def update_quality(item), do: update_quality(item, 1)

  def update_quality(%{name: name, sell_in: sell_in} = item, factor) do
    case name do
      "Aged Brie"<>_ -> increase_quality(item, factor)
      "Backstage passes"<>_ -> cond do
        sell_in <= 0 -> %{item | quality: 0}
        sell_in < 6  -> increase_quality(item, 3)
        sell_in < 11 -> increase_quality(item, 2)
        true         -> increase_quality(item, 1)
      end
      "Conjured"<>_ -> decrease_quality(item, 2 * factor)
       _ -> decrease_quality(item, factor)
    end
  end

  def update_item(%{name: "Sulfuras"<>_} = item), do: item
  def update_item(item), do: item |> update_quality |> update_sell_in

  def update_items(items), do: Enum.map(items, &update_item/1)

end
