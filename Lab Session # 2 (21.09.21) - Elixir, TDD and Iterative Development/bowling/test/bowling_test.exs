defmodule BowlingTest do
  use ExUnit.Case

  test "gutter game" do
    game = List.duplicate([0, 0], 9) ++ [[0, 0, nil]]
    assert Bowling.score(game) == 0
  end

  test "'all ones' game" do
    game = List.duplicate([1, 1], 9) ++ [[1, 1, nil]]
    assert Bowling.score(game) == 20
  end

  test "one spare" do
    game = [[5,5],[3,0]] ++ List.duplicate([0, 0], 7) ++ [[0, 0, nil]]
    assert Bowling.score(game) == 16
  end

  test "one strike" do
    game = [[10,nil],[3,4]] ++ List.duplicate([0, 0], 7) ++ [[0, 0, nil]]
    assert Bowling.score(game) == 24
  end

  test "perfect game" do
    game = List.duplicate([10,nil], 9) ++ [[10,10,10]]
    assert Bowling.score(game) == 300
  end

  test "first bad round" do
    game = [2, 3] ++ List.duplicate([10,nil], 8) ++ [[10,10,10]]
    assert Bowling.score(game) == 275
  end

  test "nine perfect frames" do
    game = List.duplicate([10, nil], 9) ++ [[3, 2, nil]]
    assert Bowling.score(game) == 253
  end

  # Some extra tests added in addition to those discussed in the lab session
  # You can add more and fix the code accordingly if you find any bugs :)

  test "almost perfect game" do
    game = List.duplicate([10, nil], 9) ++ [[10, 3, 2]]
    assert Bowling.score(game) == 278
  end

  test "three spares after first frame" do
    game = [1, 1] ++ [7, 3] ++ List.duplicate([2, 2], 6) ++ [2, 8] ++ [[7, 3, 10]]
    assert Bowling.score(game) == 75
  end

  test "two strikes after first frame" do
    game = [1, 1] ++ [10, nil] ++ List.duplicate([2, 2], 6) ++ [10, nil] ++ [[7, 1, nil]]
    assert Bowling.score(game) == 66
  end

  test "spares with perfect hit in second roll" do
    game = List.duplicate([0, 10], 9) ++ [[0, 10, 10]]
    assert Bowling.score(game) == 110
  end

  test "combining spares and strikes after first frame" do
    game = [[4, 5], [7, 3], [10, nil], [0, 10], [2, 8], [4, 1], [10, nil], [0, 1], [10, nil], [7, 3, 4]]
    assert Bowling.score(game) == 126
  end



end
