defmodule Bowling do

  def score(game) do
    List.flatten(game)
    |> Enum.filter(fn e -> is_number(e) end)    # &is_number/1
    |> full_score
  end

  defp full_score(game), do: Enum.reduce(game, 0, fn n, acc -> n + acc end) + bonus(game, 1)

  defp bonus([s1, s2, _], 10) when s1 + s2 >= 10, do: 0
  defp bonus([10 | [s2 | tail]], f), do: s2 + List.first(tail) + bonus([s2 | tail], f + 1)
  defp bonus([s1 | [s2 | tail]], f) when s1 + s2 == 10, do: List.first(tail) + bonus(tail, f + 1)
  defp bonus([_ | [_ | tail]], f), do: bonus(tail, f + 1)
  defp bonus([], _), do: 0

  # [10, nil], [3, 2, nil] -> [10, 3, 2] - problem with final matching (1 element from frame 9 and 2 elements from frame 10)
  # [10, nil], [10, 3, 2] ->  [10, 3, 2] - okay with final matching (all elements in frame 10)

end
